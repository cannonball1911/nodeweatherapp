const rpn = require('request-promise-native')

let getWeather = (latitude, longitude, language, units) => {
    const apiKey = '5b054da51cefeb41bd9511ef68eddd1a'
    return new Promise((resolve, reject) => {
        rpn({
            url: `https://api.darksky.net/forecast/${apiKey}/${latitude},${longitude}?lang=${language}&units=${units}&exclude=minutely,hourly,daily,alerts`,
            json: true
        }).then((res) => {
            resolve({
                temperature: res.currently.temperature,
                apparentTemperature: res.currently.apparentTemperature,
                summary: res.currently.summary,
                usedUnit: res.flags.units
            })
        }).catch(() => {
            reject('Unable to fetch weather!')
        })
    })
}

module.exports.getWeather = getWeather