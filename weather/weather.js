const request = require('request')

let getWeather = (latitude, longitude, language, units, callback) => {
    request({
        url: `https://api.darksky.net/forecast/5b054da51cefeb41bd9511ef68eddd1a/${latitude},${longitude}?lang=${language}&units=${units}&exclude=minutely,hourly,daily,alerts`,
        json: true
    }, (error, response, body) => {
        if (!error && response.statusCode === 200) {
            callback(undefined, {
                temperature: body.currently.temperature,
                apparentTemperature: body.currently.apparentTemperature,
                summary: body.currently.summary,
                usedUnit: body.flags.units
            })
        } else {
            callback('Unable to fetch weather!')
        }
    })
}

module.exports.getWeather = getWeather