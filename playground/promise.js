var asynAdd = (a, b) => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            (typeof a === 'number' && typeof b === 'number') ? resolve(a + b) : reject('Args must be numbers')
        }, 1500)
    })
}

asynAdd(5 ,7).then((res) => {
    console.log('Result: ', res)
    return asynAdd(res, 33)
}).then((res) => {
    console.log('Should 45', res)
}).catch((errorMessage) => {
    console.log(errorMessage)
})


// var somePromise = new Promise((resolve, reject) => {
//     setTimeout(() => {
//         // resolve('Jo geht')
//         reject('Unable to fulfill promise')
//     }, 2500)
// })

// somePromise.then((message) => {
//     console.log('Success: ', message)
// }, (errorMessage) => {
//     console.log('ERROR: ', errorMessage)
// })