const rpn = require('request-promise-native')

let geocodeAddress = (address) => {
    const encodedAddress = encodeURIComponent(address)
    const appID = 'QflWAHK4hiaimUGeKNrT'
    const appCode = 'OD0xhx-9Z9s5wR526qXf8w'
    return new Promise((resolve, reject) => {
        rpn({
            url: `https://geocoder.api.here.com/6.2/geocode.json?app_id=${appID}&app_code=${appCode}&searchtext=${encodedAddress}`,
            json: true
        }).then((res) => {
            if (res.Response.View.length === 0) {
                throw new Error('Address not found!')
            }
            resolve({
                fullAddress: res.Response.View[0].Result[0].Location.Address.Label,
                latitude: res.Response.View[0].Result[0].Location.NavigationPosition[0].Latitude,
                longitude: res.Response.View[0].Result[0].Location.NavigationPosition[0].Longitude
            })
        }).catch((err) => {
            if (err.statusCode === 400) {
                reject('Request is not valid! (Input parameter validation failed. Geocoder: missing mandatory parameters at least one of "country, state, county, city, district, street, housenumber, postalCode" or "searchtext"')
            } else if (err.message === 'Address not found!') {
                reject('Address not found!')
            } else {
                reject('Undefined error!')
            }
        })
    })
}

module.exports.geocodeAddress = geocodeAddress