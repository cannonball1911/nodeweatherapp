const request = require('request')

let geocodeAddress = (address, callback) => {
    const encodedAddress = encodeURIComponent(address)
    const appID = 'QflWAHK4hiaimUGeKNrT'
    const appCode = 'OD0xhx-9Z9s5wR526qXf8w'
    request({
        url: `https://geocoder.api.here.com/6.2/geocode.json?app_id=${appID}&app_code=${appCode}&searchtext=${encodedAddress}`,
        json: true
    }, (error, response, body) => {
        if (error) {
            callback('Unable to connect to the here.com servers!')
        }  else if (response.statusCode === 400) {
            callback(body.Details)
        } else if (body.Response.View.length === 0) {
            callback('Unable to find address!')
        } else {
            callback(undefined, {
                fullAddress: body.Response.View[0].Result[0].Location.Address.Label,
                latitude: body.Response.View[0].Result[0].Location.NavigationPosition[0].Latitude,
                longitude: body.Response.View[0].Result[0].Location.NavigationPosition[0].Longitude
            })
        }
    })
}

module.exports.geocodeAddress = geocodeAddress