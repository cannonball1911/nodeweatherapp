const yargs = require('yargs')
const colors = require('colors')
const _ = require('lodash')

const myFuncs = require('./myFuncs.js')
const geocode = require('./geocode/geocode.js')
const weather = require('./weather/weather.js')

const argv = yargs
    .options({
        address: {
            demand: true,
            alias: 'a',
            describe: 'Address to fetch weather for',
            string: true
        },
        language: {
            demand: false,
            alias: 'l',
            describe: 'Language in which the weather summary should be displayed',
            string: true,
            default: 'en'
        },
        units: {
            alias: 'u',
            describe: 'Units in which the weather conditions should be displayed',
            string: true,
            default: 'auto'
        }
    })
    .help()
    .alias('help', 'h')
    .argv

console.clear()

geocode.geocodeAddress(argv.address, (errorMessage, geocodeResults) => {
    if (errorMessage) {
        myFuncs.lError(errorMessage)
    } else {
        weather.getWeather(geocodeResults.latitude, geocodeResults.longitude, argv.language, argv.units, (errorMessage, weatherResults) => {
            if (errorMessage) {
                myFuncs.lError(errorMessage)
            } else {
                var unit;
                weatherResults.usedUnit === 'si' ? unit = '°C'.green.bold : unit = '°F'.green.bold

                const address = colors.green(geocodeResults.fullAddress).bold
                const roundedTemp = colors.green(_.round(weatherResults.temperature, 0)).bold
                const roundedApparentTemp = colors.green(_.round(weatherResults.apparentTemperature, 0)).bold
                
                console.log('--------------------Wetter--------------------'.blue)
                console.log(`Wetter für: ${address}`)
                console.log(`Die Temperatur beträgt ${roundedTemp}${unit}. (Gefühlt: ${roundedApparentTemp}${unit})`)
                console.log(`${weatherResults.summary}`)
                console.log('--------------------Wetter--------------------'.blue)
                console.log()
            }
        })
    }
})