const yargs = require('yargs')
const colors = require('colors')
const _ = require('lodash')

const myFuncs = require('./myFuncs.js')
const geocode = require('./geocode/geocode-promise.js')
const weather = require('./weather/weather-promise.js')

const argv = yargs
    .options({
        address: {
            demand: true,
            alias: 'a',
            describe: 'Address to fetch weather for',
            string: true
        },
        language: {
            demand: false,
            alias: 'l',
            describe: 'Language in which the weather summary should be displayed',
            string: true,
            default: 'en'
        },
        units: {
            alias: 'u',
            describe: 'Units in which the weather conditions should be displayed',
            string: true,
            default: 'auto'
        }
    })
    .help()
    .alias('help', 'h')
    .argv

console.clear()

let address;

geocode.geocodeAddress(argv.address).then((res) => {
    address = colors.green(res.fullAddress).bold
    return weather.getWeather(res.latitude, res.longitude, argv.language, argv.units)
}).then((res) => {
    var unit;
    res.usedUnit === 'si' ? unit = '°C'.green.bold : unit = '°F'.green.bold

    const roundedTemp = colors.green(_.round(res.temperature, 0)).bold
    const roundedApparentTemp = colors.green(_.round(res.apparentTemperature, 0)).bold

    console.log('--------------------Wetter--------------------'.blue)
    console.log(`Wetter für: ${address}`)
    console.log(`Die Temperatur beträgt ${roundedTemp}${unit}. (Gefühlt: ${roundedApparentTemp}${unit})`)
    console.log(`${res.summary}`)
    console.log('--------------------Wetter--------------------'.blue)
    console.log()
}).catch((err) => {
    myFuncs.lError(err)
})