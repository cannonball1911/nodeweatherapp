// Just some console functions

const colors = require('colors')

var lError = (errorMessage) => {
    console.log(`[ERROR]: ${errorMessage}`.red)
}

module.exports.lError = lError